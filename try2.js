(function() {
  var casper, nextPageLink, patientInfos, processedLinks, url;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  patientInfos = [];

  processedLinks = [];

  nextPageLink = '';

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    verbose: true,
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  url = 'http://www.shilehui.com/A9093.htm';

  casper.start(url, function() {
    return console.log('started');
  });

  casper.then(function() {
    var patientInfo;
    return patientInfo = this.evaluate(function() {
      var userInfoList;
      patientInfo = {};
      userInfoList = $('#Left_Assist1_spanReceipt tr');
      userInfoList.each(function(i, el) {
        var addrTitle, ageTitle, cellphoneTitle, genderTitle, nameTitle, qqTitle, securityIdTitle;
        nameTitle = $(el).find('td:contains("真实姓名")');
        if (nameTitle.length > 0) patientInfo.name = nameTitle.next('td').text();
        securityIdTitle = $(el).find('td:contains("证")');
        if (securityIdTitle.length > 0) {
          patientInfo.securityId = securityIdTitle.next('td').text();
        }
        genderTitle = $(el).find('td:contains("性")');
        if (genderTitle.length > 0) {
          patientInfo.gender = genderTitle.next('td').text();
        }
        ageTitle = $(el).find('td:contains("龄")');
        if (ageTitle.length > 0) patientInfo.age = ageTitle.next('td').text();
        cellphoneTitle = $(el).find('td:contains("机")');
        if (cellphoneTitle.length > 0) {
          patientInfo.cellphone = cellphoneTitle.next('td').text();
        }
        addrTitle = $(el).find('td:contains("址")');
        if (addrTitle.length > 0) patientInfo.addr = addrTitle.next('td').text();
        qqTitle = $(el).find('td:contains("Q")');
        if (qqTitle.length > 0) return patientInfo.qq = qqTitle.next('td').text();
      });
      return console.log(JSON.stringify(patientInfo));
    });
  });

  casper.run(function() {
    return this.exit();
  });

}).call(this);

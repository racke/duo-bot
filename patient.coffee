Ext.onReady ->
    Ext.QuickTips.init()
    Ext.define('Patient',
        extend: 'Ext.data.Model'
        fields: ['id','name','cellphone','desc','url','qq','releaseDate','source']
    )
    window.store = Ext.create('Ext.data.Store',
        pageSize: 25
        model: 'Patient'
        proxy:
            type: 'ajax'
            url: 'index.php?r=patient/getPatients'
            reader:
                type: 'json'
                root: 'patients'
                totalProperty: 'size'
    )
    mask = null
    contactAction = Ext.create('Ext.Action',
        icon: './images/accept.png'
        disabled: true
        handler: (widget, e)->
            rec = grid.getSelectionModel().getSelection()[0]
            if(rec)
                #console.log rec
                Ext.MessageBox.confirm('确认', '确认已经联系 '+rec.data.name+' ?', (btn, text)->
                    #console.log(btn)
                    if btn == 'no'
                        #console.log 'is not yes'
                        return
                    #if not mask
                    #mask = new Ext.LoadMask(Ext.getBody(), {msg: '确认中'})
                    #mask.show()
                    Ext.getBody().mask( '确认中')
                    Ext.Ajax.request
                        url: 'index.php?r=patient/contactPatient&id='+rec.data.id
                        success: (resp)->
                            #mask.hide()
                            Ext.getBody().unmask()
                            window.store.load()

                )
    )
    grid = Ext.create('Ext.grid.Panel',
        store: window.store
        columns: [
            {text:"名字", dataIndex:'name'}
            {text:"联系", dataIndex:'cellphone'}
            {text:"QQ", dataIndex:'qq'}
            {text:"描述", dataIndex:'desc', width:500}
            {text:"链接", dataIndex:'url', width:200, renderer: (val)->
                return '<a href="'+val+'" target="_blank">'+val+'</a>'}
            {text:"加入日期", dataIndex:'releaseDate'}
            {text:"来源", dataIndex:'source'}
        ]
        height: 600
        bbar: Ext.create('Ext.PagingToolbar',
            store: window.store
            displayInfo: true
            displayMsg: '显示 {0} - {1} of {2}'
            emptyMsg: '无'
        )
        dockedItems:[
            {
                xtype: 'toolbar'
                items:[contactAction]
            }
        ]
        viewConfig:
            enableTextSelection: true
        renderTo: 'grid'
    )
    grid.getSelectionModel().on(
        selectionchange: (sm, selections)->
            if(selections.length)
                contactAction.setText('联系完 '+selections[0].data.name+' 请点我, 避免重复联系')
                contactAction.enable()

            return false
    )
    #select the first row
    window.store.on('load', (store, records)->
        if records.length > 0
            grid.getSelectionModel().select(0)
            grid.fireEvent('rowclick', grid, 0)
        if not window.progressBar
            window.progressBar = Ext.create('Ext.ProgressBar',
                renderTo: Ext.get('proBar')
            )
        contacted = window.store.proxy.reader.jsonData.contactedSize
        total = window.store.proxy.reader.jsonData.size
        progressBar.updateProgress(contacted/(total+contacted), "进度: 已联系 "+contacted + " 剩余 "+total, true)
    ,this, {single: false})
    window.store.load()

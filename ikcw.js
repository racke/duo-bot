(function() {

  window.opts = {
    baseUrl: 'http://www.ikcw.com',
    indexPath: '/jiuzhu/'
  };

  opts.pageLimit = 5;

  opts.getPatientLinks = function() {
    var links;
    links = [];
    console.log('getPatientLinks');
    console.log($('a[class=clz_wyqz_title]'));
    $('a[class=clz_wyqz_title]').each(function(i, el) {
      var href;
      href = $(el).attr('href');
      href = href.substring(2);
      links.push(href);
      return $(el).removeAttr('target');
    });
    return links;
  };

  opts.getPaginationNextLink = function() {
    var isDisabled, nextPageEl;
    nextPageEl = $('a:contains(下一页)');
    if (!nextPageEl) return false;
    nextPageEl.attr('id', 'nextPage');
    isDisabled = nextPageEl.attr('disabled');
    console.log('isDisabled: ' + isDisabled);
    if (isDisabled) return false;
    return '#nextPage';
  };

  opts.getPatientInfo = function() {
    var date, patientInfo, userInfoList;
    patientInfo = {};
    patientInfo.source = '癌症求助網';
    date = $('.career_detail_left_date').text().substring(0, 9);
    patientInfo.releaseDate = date;
    userInfoList = $('.cishangongdebang_gg:first tr');
    userInfoList.each(function(i, el) {
      var addrTitle, ageTitle, cellphoneTitle, genderTitle, nameTitle, qqTitle, securityIdTitle;
      nameTitle = $(el).find('td:contains("名")');
      if (nameTitle.length > 0) patientInfo.name = nameTitle.next('td').text();
      securityIdTitle = $(el).find('td:contains("证")');
      if (securityIdTitle.length > 0) {
        patientInfo.securityId = securityIdTitle.next('td').text();
      }
      genderTitle = $(el).find('td:contains("性")');
      if (genderTitle.length > 0) {
        patientInfo.gender = genderTitle.next('td').text();
      }
      ageTitle = $(el).find('td:contains("龄")');
      if (ageTitle.length > 0) patientInfo.age = ageTitle.next('td').text();
      cellphoneTitle = $(el).find('td:contains("手")');
      if (cellphoneTitle.length > 0) {
        patientInfo.cellphone = cellphoneTitle.next('td').text();
      }
      cellphoneTitle = $(el).find('td:contains("话")');
      if (cellphoneTitle.length > 0) {
        patientInfo.cellphone = cellphoneTitle.next('td').text();
      }
      addrTitle = $(el).find('td:contains("址")');
      if (addrTitle.length > 0) patientInfo.addr = addrTitle.next('td').text();
      qqTitle = $(el).find('td:contains("Q")');
      if (qqTitle.length > 0) patientInfo.qq = qqTitle.next('td').text();
      patientInfo.desc = $('.career_detail_left_title').text();
      return patientInfo.url = document.location.href;
    });
    console.log(JSON.stringify(patientInfo));
    return patientInfo;
  };

  console.log('ikcw config is loaded');

}).call(this);

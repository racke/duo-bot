#path to your casper js https://github.com/n1k0/casperjs 
phantom.casperPath = '../casperjs'
phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js')
titles = []
titleEls = []
casper = require('casper').create(
    clientScripts: [
        'libs/jquery-1.7.1.min.js'
    ]
    logLevel: 'info'
    verbose: 'true'
    onError: (self, m)->
        console.log 'FATAL:' + m
        self.exit()
    pageSetting:
        loadImages: false
        loadPlugins: false

)
console.log 'casper created'
#url = 'http://google.com/'
url = 'http://127.0.0.1/mobilePoc/index.html'
casper.start(url, ()->
    #@.fill('form[name=f]', {q: 'casperjs'}, true)
)
link = ''
casper.then(->
    link = casper.evaluate(()->
        titles = []
        link = $('#test').attr('href')
        return link
    )
)
casper.thenOpen('http://127.0.0.1/mobilePoc/'+link, ->
    console.log @.getTitle()
)
###
casper.then ->
    link = link
    @.click "a[href='#{link}']"
    @.wait 100, ->
        @.evaluate ->
            console.log document.title
###
casper.run(->
    console.log link
    @.exit()
)

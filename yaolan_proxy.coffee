#To trim the reidrect script in yaolan forum
http = require 'http'

server = http.createServer( (request, response)->
    console.log 'server created'
    host = request.headers['host']
    if host == '127.0.0.1:8080' then host = 'bbs.yaolan.com'
    console.log 'host is '+ host
    options =
        #host: 'bbs.yaolan.com'
        host: host
        #host: 'www.douban.com'
        port: 80
        path: request.url
        method: 'GET'
    proxy_request = http.request(options, (proxy_response)->
        console.log 'porxy response callback'
        #proxy_response.setEncoding('utf8')
        str_resp = ''
        proxy_response.on('data', (chunk)->
            console.log 'data received'
            str_resp += chunk
        )
        proxy_response.on('end', ()->
            #truncate all code after the </html>
            str_resp = str_resp.substring(0, str_resp.indexOf('<\/html>')+7)
            str_resp = str_resp.replace('document.domain', '//document.domain')
            contentType = proxy_response.headers['content-type']
            console.log JSON.stringify(proxy_response.headers)
            console.log 'content type '+contentType
            #console.log str_resp
            response.writeHead(200,
                'Content-Type' : contentType
            )
            response.write(str_resp, 'utf8')
            response.end()
        )
    )
    proxy_request.on('error', ->
        console.log 'request error'
    )
    proxy_request.end()
).listen(8080)
server.on('clientError', ->
    console.log 'server client error'
)

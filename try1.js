(function() {
  var casper, titleEls, titles, url;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  titles = [];

  titleEls = [];

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  url = 'http://127.0.0.1/mobilePoc/index.html';

  casper.start(url, function() {});

  console.log('titles ' + titles);

  casper.then(function() {
    return titles = casper.evaluate(function() {
      titles = [];
      titleEls = $('h3.r a');
      titleEls.each(function(i, el) {
        return titles.push($(el).text());
      });
      return titles;
    });
  });

  casper.run(function() {
    return console.log(titles);
  });

}).call(this);

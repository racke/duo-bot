#path to your casper js https://github.com/n1k0/casperjs 
phantom.casperPath = 'casperjs'
phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js')
#parentInfo {name:'', desc:'', sid:'', cellphone: '', url: ''}
casper = require('casper').create(
    clientScripts: [
        'libs/jquery-1.7.1.min.js'
    ]
    logLevel: 'info'
    verbose: true
    onError: (self, m)->
        console.log 'FATAL:' + m
        self.exit()
    pageSetting:
        loadImages: false
        loadPlugins: false

)
console.log 'casper created'
console.log 'passed config file '
util = require("utils")
#start args
configFile = casper.cli.options.config

console.log 'get configs from '+configFile
phantom.injectJs(configFile)
util.dump(opts)

baseUrl = opts.baseUrl
pageLimit = opts.pageLimit
indexPage = baseUrl
if opts.indexPath
    indexPath = opts.indexPath
    indexPage = baseUrl + indexPath
console.log 'index Page: '+indexPage
getPaginationNextLink = opts.getPaginationNextLink
getPatientInfo = opts.getPatientInfo
getPatientLinks = opts.getPatientLinks

#vars
patientInfos = []
processedLinks = []
nextPageLink = ''
nextPageImg = ''
donateMain = ''
needExit = false
pageIdx = 0

#start scrape
casper.start(indexPage, ()->
    console.log 'started'
)
casper.then(->
    getPatientInfosInList(true)
)
openPatientLinksInPage= ->
    console.log 'openPatientLinksInPage'
    patientLinks = casper.evaluate getPatientLinks
    console.log 'after links returned'
    console.log patientLinks.length
    #console.log 'patient links size is'.patientLinks.length
    casper.then ->
        patientLinks.forEach (link, i)->
            #console.log 'link '.link
            #to debug
            #if i>1 then return
            casper.then ->
                console.log 'opening patient link '+link
                patientUrl = baseUrl + link
                if baseUrl == 'http://127.0.0.1:8080' then patientUrl= 'http://bbs.yaolan.com'+link
                casper.thenOpen(patientUrl, ->
                    console.log 'now in page' + @.getTitle()
                    patientInfos = patientInfos.concat(casper.evaluate(getPatientInfo))
                )

postProcess = ->
    pageIdx++
    console.log 'page index is '+pageIdx
    if pageIdx >= pageLimit then needExit = true
    console.log 'exit '+ needExit
    if not needExit
        casper.then ->
            getPatientInfosInList()

getPatientInfosInList = (isStart=false)->
    if not isStart
        casper.then ->
            #To get next page's link
            if nextPageLink
                nextPageLink = baseUrl + nextPageLink
                console.log 'next page: '+ nextPageLink
                casper.thenOpen nextPageLink, ->
                    nextPageLink = @.evaluate(getPaginationNextLink)
                    openPatientLinksInPage()
            else
                console.log 'no next page links, exiting...'
                needExit = true
            postProcess()
    else
        casper.then ->
            nextPageLink = @.evaluate(getPaginationNextLink)
            console.log 'nextPage links is '+nextPageLink
            openPatientLinksInPage()
            postProcess()
#server_base = 'http://127.0.0.1'
server_base = 'http://w74164.s74.chinaccnet.cn'
casper.then(->
    #strPatientInfos = encodeURIComponent(JSON.stringify(patientInfos))
    strPatientInfos = JSON.stringify(patientInfos)
    console.log(strPatientInfos)
    casper.open(server_base+'/duo-bot/index.php?r=patient/createPatients', {method:'post', data:{'data':strPatientInfos}}).then ->
        @.debugHTML()
)
fs = require 'fs'
casper.run(->
    #write results to a file
    strPatientInfos = JSON.stringify(patientInfos)
    fs.touch('./output')
    resultFile = fs.open('./output', 'w')
    resultFile.write(strPatientInfos)
    resultFile.close()
    @.exit()
)

window.opts =
    baseUrl: 'http://www.shilehui.com/'
    indexPath: '/AssistUI/Index.aspx?PassType=3'
    pageLimit: 44
opts.indexPage=opts.baseUrl+'AssistUI/Index.aspx?PassType=3'
opts.getPatientLinks = ->
    links = []
    $('.donate_info_content_title a').each (i, el)->
        links.push($(el).attr('href'))
        $(el).removeAttr('target')
    
    return links

opts.getPaginationNextLink = ->
    strNextPage = ''
    nextPageImg = $('#donate_main img[src*="Page_next.gif"]')
    if nextPageImg.parent('a')
        strNextPage= nextPageImg.parent('a').attr('href')
    else
        strNextPage = ''
    if not strNextPage
        return ''
    return strNextPage

opts.getPatientInfo = ->
    window.isDoctor = (pro)->
        result = false
        for key in ['医', '放射', '血', '疗', '大夫','儿科','内科']
            if pro.indexOf(key) != -1
                result = true
                break
        return result
    patientInfo = {}
    patientInfo.source = '施乐会'
    dateTitle = $('.topicMeta').find('span:contains("发布时间")')
    date = ''
    if dateTitle.length > 0
        date = dateTitle.next('em').text()
        if date then date = date.substring(0, 9)
        patientInfo.releaseDate = date
    userInfoList = $('#Left_Assist1_spanReceipt tr')
    userInfoList.each (i, el)->
        nameTitle = $(el).find('td:contains("名")')
        if nameTitle.length > 0
            patientInfo.name = nameTitle.next('td').text()
        securityIdTitle = $(el).find('td:contains("证")')
        if securityIdTitle.length >0
            patientInfo.securityId = securityIdTitle.next('td').text()

        genderTitle = $(el).find('td:contains("性")')
        if genderTitle.length >0
            patientInfo.gender = genderTitle.next('td').text()
    
        ageTitle = $(el).find('td:contains("龄")')
        if ageTitle.length >0
            patientInfo.age = ageTitle.next('td').text()

        cellphoneTitle = $(el).find('td:contains("手")')
        #console.log('celltitle: '+cellphoneTitle.text())
        if cellphoneTitle.length >0
            patientInfo.cellphone = cellphoneTitle.next('td').text()

        cellphoneTitle = $(el).find('td:contains("话")')
        #console.log('celltitle: '+cellphoneTitle.text())
        if cellphoneTitle.length >0
            patientInfo.cellphone = cellphoneTitle.next('td').text()

        addrTitle = $(el).find('td:contains("址")')
        if addrTitle.length >0
            patientInfo.addr = addrTitle.next('td').text()

        qqTitle = $(el).find('td:contains("Q")')
        if qqTitle.length >0
            patientInfo.qq = qqTitle.next('td').text()

        patientInfo.desc = document.title
        patientInfo.url = document.location.href
    console.log  JSON.stringify patientInfo
    #get prover
    proverInfo = {}
    proverEls = $('#Left_Assist1_spanshow_wjy').parents('tr').find('tr')
    proverEls.each (i, el)->
        proTitle = $(el).find('td:contains("职")')
        pro = proTitle.next('td').text()
        cellTitle = $(el).find('td:contains("机")')
        if cellTitle
            cell = cellTitle.next('td').text()
            if cell then proverInfo.cellphone = cell
        phoneTitle = $(el).find('td:contains("话")')
        if not proverInfo.cellphone and phoneTitle
            cell = phoneTitle.next('td').text()
            if cell then proverInfo.cellphone = cell
        #console.log '==========is doctor '+window.isDoctor
        if window.isDoctor(pro)
            console.log '医生 exit!!!'
            proverInfo = {}
            return
        proverInfo.desc = '证明人 '+pro
        proverInfo.name = '无'
        proverInfo.source= '施乐会'
        proverInfo.url = document.location.href
        proverInfo.releaseDate = date
    starterInfo = {}
    starterEls = $('#Left_Assist1_spanshow').parents('tr').find('tr')
    starterEls.each (i, el)->
        proTitle = $(el).find('td:contains("职")')
        pro = proTitle.next('td').text()
        cellTitle = $(el).find('td:contains("机")')
        telTitle = $(el).find('td:contains("话")')
        #console.log '==========is doctor '+window.isDoctor
        if cellTitle
            cell = cellTitle.next('td').text()
            if cell && cell !='无' then starterInfo.cellphone = cell
        if not starterInfo.cellphone
            cell = telTitle.next('td').text()
            if cell && cell !='无' then starterInfo.cellphone = cell

        if isDoctor(pro)
            console.log '医生 exit!!!'
            starterInfo = {}
            return
        starterInfo.desc = '发起人 '+pro
        starterInfo.name = '无'
        starterInfo.source = '施乐会'
        starterInfo.url = document.location.href
        starterInfo.releaseDate = date

    return [patientInfo, proverInfo, starterInfo]

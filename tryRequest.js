(function() {
  var casper, link, titleEls, titles, url;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  titles = [];

  titleEls = [];

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    verbose: 'true',
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  url = 'http://127.0.0.1/mobilePoc/index.html';

  casper.start(url, function() {});

  link = '';

  casper.then(function() {
    var data;
    data = '';
    return casper.open('http://127.0.0.1/mobilePoc/test.php', {
      method: 'post',
      data: {
        'data': 'dsfadfdsaf'
      }
    }).then(function() {
      this.debugHTML();
      data = this.evaluate(function() {
        return $('#data').text();
      });
      return console.log('data transfered ' + data);
    });
  });

  casper.run(function() {
    return this.exit();
  });

}).call(this);

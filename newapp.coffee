#path to your casper js https://github.com/n1k0/casperjs 
phantom.casperPath = 'casperjs'
phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js')
#parentInfo {name:'', desc:'', sid:'', cellphone: '', url: ''}
util = require("utils")
casper = require('casper').create(
    clientScripts: [ 'libs/jquery-1.7.1.min.js' , 'common.js']
    logLevel: 'debug'
    verbose: true
    onResourceReceived: (req)->
        #console.log 'resource received'
        #util.dump(req)
    onError: (self, m)->
        console.log 'FATAL:' + m
        self.exit()
    pageSetting:
        loadImages: false
        loadPlugins: false
        javascriptEnabled: false

)
console.log 'casper created'
console.log 'passed config file '
#start args
configFile = casper.cli.options.config
console.log 'get configs from '+configFile
phantom.injectJs(configFile)
#util.dump(opts)

baseUrl = opts.baseUrl
indexPath = opts.indexPath
indexPage = baseUrl + indexPath
console.log 'index page: '+indexPage
pageLimit = opts.pageLimit
#pageLimit = 1
#
beforeStart = opts.beforeStart
getPaginationNextLink = opts.getPaginationNextLink
getPatientInfo = opts.getPatientInfo
getPatientLinks = opts.getPatientLinks


#vars
patientInfos = []
processedLinks = []
nextPageLink = ''
nextPageImg = ''
donateMain = ''
needExit = false
pageIdx = 0

#start scrape
casper.start(indexPage, ->
    console.log 'started'
)
if beforeStart
    casper.wait(1000, ->
        link = beforeStart(casper)
        if link then casper.click(link)
    )

casper.then ->
    casper.wait(1000, getPatientInfosInList)
    


casper.then ->
    console.log 'patient links'
    console.log patientLinks
    patientLinks.forEach (link, i)->
        #to debug
        #if i>1 then return
        casper.then ->
            
            url = link
            if link.indexOf('http') == -1
                url = baseUrl + link
            if baseUrl == 'http://127.0.0.1:8080' then url = 'http://bbs.yaolan.com/'+link
            console.log 'opening patient link '+url
            casper.thenOpen(url, ->
                console.log 'now in page' + @.getTitle()
                patientInfos.push(casper.evaluate(getPatientInfo))
                if patientInfos.length == 10
                    sendToServer()
                    patientInfos = []
            )

###    
getNextPageLink= ->
    $('a:contains(下一页)').attr('id', 'nextPage')
    isDisabled = $('a:contains(下一页)').attr('disabled')
    console.log 'isDisabled: '+isDisabled
    if isDisabled then return false
    return '#nextPage'
###

patientLinks = []
getPatientInfosInList = (isStart=false)->
    pageIdx++
    console.log 'page index is '+pageIdx
    patientLinks = patientLinks.concat(casper.evaluate(getPatientLinks))
    nextPageLink = casper.evaluate(getPaginationNextLink)
    console.log 'nextPageLink is '+nextPageLink
    #end the recurse
    if nextPageLink is false or pageIdx>pageLimit
        console.log 'no next page or exceed page limit, end paging'
        return
    console.log 'click '+nextPageLink
    casper.click(nextPageLink)
    casper.wait 1000, ->
        getPatientInfosInList()


#server_base = 'http://127.0.0.1'
server_base = 'http://w74164.s74.chinaccnet.cn'
sendToServer = ->
    strPatientInfos = JSON.stringify(patientInfos)
    console.log(strPatientInfos)
    casper.open(server_base+'/duo-bot/index.php?r=patient/createPatients', {method:'post', data:{'data':strPatientInfos}}).then ->
        @.debugHTML()

casper.then(->
    #strPatientInfos = encodeURIComponent(JSON.stringify(patientInfos))
    sendToServer()
)
#fs = require 'fs'
casper.run(->
    #write results to a file
    #strPatientInfos = JSON.stringify(patientInfos)
    #fs.touch('./output')
    #resultFile = fs.open('./output', 'w')
    #resultFile.write(strPatientInfos)
    #resultFile.close()
    @.exit()
)

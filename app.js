(function() {
  var baseUrl, casper, configFile, donateMain, fs, getPaginationNextLink, getPatientInfo, getPatientInfosInList, getPatientLinks, indexPage, indexPath, needExit, nextPageImg, nextPageLink, openPatientLinksInPage, pageIdx, pageLimit, patientInfos, postProcess, processedLinks, server_base, util;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    verbose: true,
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  console.log('passed config file ');

  util = require("utils");

  configFile = casper.cli.options.config;

  console.log('get configs from ' + configFile);

  phantom.injectJs(configFile);

  util.dump(opts);

  baseUrl = opts.baseUrl;

  pageLimit = opts.pageLimit;

  indexPage = baseUrl;

  if (opts.indexPath) {
    indexPath = opts.indexPath;
    indexPage = baseUrl + indexPath;
  }

  console.log('index Page: ' + indexPage);

  getPaginationNextLink = opts.getPaginationNextLink;

  getPatientInfo = opts.getPatientInfo;

  getPatientLinks = opts.getPatientLinks;

  patientInfos = [];

  processedLinks = [];

  nextPageLink = '';

  nextPageImg = '';

  donateMain = '';

  needExit = false;

  pageIdx = 0;

  casper.start(indexPage, function() {
    return console.log('started');
  });

  casper.then(function() {
    return getPatientInfosInList(true);
  });

  openPatientLinksInPage = function() {
    var patientLinks;
    console.log('openPatientLinksInPage');
    patientLinks = casper.evaluate(getPatientLinks);
    return casper.then(function() {
      return patientLinks.forEach(function(link, i) {
        return casper.then(function() {
          var patientUrl;
          console.log('opening patient link ' + link);
          patientUrl = baseUrl + link;
          if (baseUrl === 'http://127.0.0.1:8080') {
            patientUrl = 'http://bbs.yaolan.com' + link;
          }
          return casper.thenOpen(patientUrl, function() {
            console.log('now in page' + this.getTitle());
            return patientInfos = patientInfos.concat(casper.evaluate(getPatientInfo));
          });
        });
      });
    });
  };

  postProcess = function() {
    pageIdx++;
    console.log('page index is ' + pageIdx);
    if (pageIdx >= pageLimit) needExit = true;
    console.log('exit ' + needExit);
    if (!needExit) {
      return casper.then(function() {
        return getPatientInfosInList();
      });
    }
  };

  getPatientInfosInList = function(isStart) {
    if (isStart == null) isStart = false;
    if (!isStart) {
      return casper.then(function() {
        if (nextPageLink) {
          nextPageLink = baseUrl + nextPageLink;
          console.log('next page: ' + nextPageLink);
          casper.thenOpen(nextPageLink, function() {
            nextPageLink = this.evaluate(getPaginationNextLink);
            return openPatientLinksInPage();
          });
        } else {
          console.log('no next page links, exiting...');
          needExit = true;
        }
        return postProcess();
      });
    } else {
      return casper.then(function() {
        nextPageLink = this.evaluate(getPaginationNextLink);
        console.log('nextPage links is ' + nextPageLink);
        openPatientLinksInPage();
        return postProcess();
      });
    }
  };

  server_base = 'http://w74164.s74.chinaccnet.cn';

  casper.then(function() {
    var strPatientInfos;
    strPatientInfos = JSON.stringify(patientInfos);
    console.log(strPatientInfos);
    return casper.open(server_base + '/duo-bot/index.php?r=patient/createPatients', {
      method: 'post',
      data: {
        'data': strPatientInfos
      }
    }).then(function() {
      return this.debugHTML();
    });
  });

  fs = require('fs');

  casper.run(function() {
    var resultFile, strPatientInfos;
    strPatientInfos = JSON.stringify(patientInfos);
    fs.touch('./output');
    resultFile = fs.open('./output', 'w');
    resultFile.write(strPatientInfos);
    resultFile.close();
    return this.exit();
  });

}).call(this);

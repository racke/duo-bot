(function() {

  window.opts = {
    baseUrl: 'http://www.happyd.com',
    indexPath: '/help/hlist/'
  };

  opts.pageLimit = 0;

  opts.beforeStart = function(casper) {
    var linkId;
    console.log('started');
    linkId = casper.evaluate(function() {
      var link;
      console.log('next is' + $('.pageSplitNext')[0]);
      $('.pageSplitNext').attr('id', 'nextPage');
      link = $('a:contains("助医")');
      console.log('link is ' + link);
      if (link) {
        link.attr('id', 'medicine');
        console.log($(link).attr('id'));
        return 'medicine';
      }
      return false;
    });
    if (linkId) return '#medicine';
  };

  opts.getPatientLinks = function() {
    var links;
    links = [];
    console.log('getPatientLinks');
    console.log($('.items .dec a'));
    $('.items .dec a.green').each(function(i, el) {
      var href;
      href = $(el).attr('href');
      links.push(href);
      return $(el).removeAttr('target');
    });
    return links;
  };

  opts.getPaginationNextLink = function() {
    var nextPageEl;
    nextPageEl = $('.pageSplitNext');
    console.log('next page el ' + nextPageEl[0]);
    if (!nextPageEl) return false;
    nextPageEl.attr('id', 'nextPage');
    return '#nextPage';
  };

  opts.getPatientInfo = function() {
    var cellTitles, cellnumber, date, idx, name, patientInfo;
    patientInfo = {};
    patientInfo.source = '開心點';
    date = $('.article-con .hd p').text().match('\\d{4}-\\d{1,2}-\\d{1,2}')[0];
    patientInfo.releaseDate = date;
    name = $('#hname').text();
    if (!name) name = "无";
    patientInfo.name = name;
    cellTitles = $('#info_2 *:contains("机")');
    idx = cellTitles.length - 1;
    cellnumber = $(cellTitles[idx]).text().match(/1[3|4|5|8][0-9]\d{4,8}/)[0];
    if (!cellnumber || cellnumber.length !== 11) {
      console.log('unable to get cell for ' + name);
      return null;
    }
    patientInfo.cellphone = cellnumber;
    patientInfo.url = document.location.href;
    patientInfo.desc = $('#sTtile').text();
    console.log(JSON.stringify(patientInfo));
    return patientInfo;
  };

  console.log('happyd config is loaded');

}).call(this);

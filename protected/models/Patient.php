<?php

/**
 * This is the model class for table "patient".
 *
 * The followings are the available columns in table 'patient':
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property string $cellphone
 * @property string $securityId
 * @property string $source
 * @property string $age
 * @property integer $isContacted
 * @property string $addr
 * @property string $gender
 * @property string $qq
 * @property string $email
 * @property string $url
 * @property string $releaseDate
 */
class Patient extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Patient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'patient';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, cellphone, releaseDate', 'required'),
			array('isContacted', 'numerical', 'integerOnly'=>true),
			array('name, cellphone, source, age, gender, qq, email', 'length', 'max'=>32),
			array('desc, addr', 'length', 'max'=>256),
			array('securityId, url', 'length', 'max'=>64),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, desc, cellphone, securityId, source, age, isContacted, addr, gender, qq, email, url, releaseDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'desc' => 'Desc',
			'cellphone' => 'Cellphone',
			'securityId' => 'Security',
			'source' => 'Source',
			'age' => 'Age',
			'isContacted' => 'Is Contacted',
			'addr' => 'Addr',
			'gender' => 'Gender',
			'qq' => 'Qq',
			'email' => 'Email',
			'url' => 'Url',
			'releaseDate' => 'Release Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('cellphone',$this->cellphone,true);
		$criteria->compare('securityId',$this->securityId,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('age',$this->age,true);
		$criteria->compare('isContacted',$this->isContacted);
		$criteria->compare('addr',$this->addr,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('qq',$this->qq,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('releaseDate',$this->releaseDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

class PatientController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','createPatients', 'getPatients','contactPatient'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Patient;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Patient']))
		{
			$model->attributes=$_POST['Patient'];
      Yii::log(CVarDumper::dumpAsString($_POST['Patient']), 'info');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
    public function actionContactPatient(){
        if(isset($_REQUEST['id'])){
           $patientId = $_REQUEST['id']; 
           $model = $this->loadModel($patientId);
           $model->setAttribute('isContacted', 1);
           $model->save();
           echo '{"success": true}';
        }
        if(isset($_REQUEST['ids'])){
            $patientIds = explode(',', $_REQUEST['ids']);
            foreach($patientIds as $patientId){
                $model = $this->loadModel($patientId);
                $model->setAttribute('isContacted', 1);
                $model->save();
            }
           echo '{"success": true}';
        }
    
    }
	public function actionCreatePatients()
	{
    if(isset($_REQUEST['data']))
    {
        $patients = CJSON::decode($_REQUEST['data']);
        Yii::log(CVarDumper::dumpAsString($patients), 'info');
        //Yii::log($patients, 'info');
        echo $patients;
        foreach($patients as $patient){
            $model = new Patient;
            if(!isset($patient['cellphone'])){
                Yii::log('cellphone is empty, pls ignore');
                continue;
            }

            if($model->exists('cellphone="'.$patient['cellphone'].'"')){
                Yii::log('patient with cellphone '.$patient['cellphone'].' already exists, ignore...');
                continue;
            }
            Yii::log(CVarDumper::dumpAsString($patient), 'info');
            $model->attributes=$patient;
            $status = $model->save();
            Yii::log('saving patient '.$model->id.' status '.$status);
            if($status == false){
                Yii::log('error: '.CVarDumper::dumpAsString($model->errors));
            }
        }
    }
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Patient']))
		{
			$model->attributes=$_POST['Patient'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Patient');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
    public function actionGetPatients(){
        $page = 0;
        $pageSize = 25;
        if(isset($_GET['page'])){
            $page = $_GET['page'];
            if($page>=1) $page--;
        }
        if(isset($_GET['pageSize'])){
            $pageSize = $_GET['pageSize'];
        }
        $dataProvider = new CActiveDataProvider('Patient', array(
            'criteria'=>array(
                'condition'=>'isContacted=0',
                'order'=>'releaseDate DESC'
            ),
            'pagination'=>array(
                'pageSize'=>$pageSize,
                'currentPage'=>$page
            ))
        );
        $contactedDP = new CActiveDataProvider('Patient', array(
            'criteria'=>array(
                'condition'=>'isContacted=1'
            )
        ));
        $patients = $dataProvider->getData();
        Yii::log('dataprovider currentPage is '.$dataProvider->getPagination()->getCurrentPage());
        Yii::log('dataprovider pageSize is '.$dataProvider->getPagination()->getPageSize());
        $result = array();
        foreach($patients as $patient){
            $result[] = $patient->getAttributes(); 
        }
        $jsonResult = CJSON::encode($result);
        $jsonResult = '{ patients: '.$jsonResult.', size:'.$dataProvider->getTotalItemCount().', contactedSize: '.$contactedDP->getTotalItemCount().'}';
        echo $jsonResult;
        Yii::log(CVarDumper::dumpAsString($jsonResult));
    }
        
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Patient('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Patient']))
			$model->attributes=$_GET['Patient'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Patient::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='patient-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

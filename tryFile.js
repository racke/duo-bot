(function() {
  var casper, fileList, fs, output, titleEls, titles, url;

  phantom.casperPath = '../casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  titles = [];

  titleEls = [];

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  url = 'http://127.0.0.1/mobilePoc/index.html';

  casper.start(url, function() {});

  console.log('titles ' + titles);

  fs = require('fs');

  fileList = fs.list('.');

  console.log(fileList);

  fs.touch('./output1');

  output = fs.open('./output1', 'w');

  output.write('hello');

  output.close();

  casper.run(function() {
    return this.exit();
  });

}).call(this);

(function() {
  var http, server;

  http = require('http');

  server = http.createServer(function(request, response) {
    var host, options, proxy_request;
    console.log('server created');
    host = request.headers['host'];
    if (host === '127.0.0.1:8080') host = 'bbs.yaolan.com';
    console.log('host is ' + host);
    options = {
      host: host,
      port: 80,
      path: request.url,
      method: 'GET'
    };
    proxy_request = http.request(options, function(proxy_response) {
      var str_resp;
      console.log('porxy response callback');
      str_resp = '';
      proxy_response.on('data', function(chunk) {
        console.log('data received');
        return str_resp += chunk;
      });
      return proxy_response.on('end', function() {
        var contentType;
        str_resp = str_resp.substring(0, str_resp.indexOf('<\/html>') + 7);
        str_resp = str_resp.replace('document.domain', '//document.domain');
        contentType = proxy_response.headers['content-type'];
        console.log(JSON.stringify(proxy_response.headers));
        console.log('content type ' + contentType);
        response.writeHead(200, {
          'Content-Type': contentType
        });
        response.write(str_resp, 'utf8');
        return response.end();
      });
    });
    proxy_request.on('error', function() {
      return console.log('request error');
    });
    return proxy_request.end();
  }).listen(8080);

  server.on('clientError', function() {
    return console.log('server client error');
  });

}).call(this);

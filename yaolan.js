(function() {

  window.opts = {
    baseUrl: 'http://127.0.0.1:8080',
    indexPath: '/board_40.aspx'
  };

  opts.pageLimit = 30;

  opts.getPatientLinks = function() {
    var links;
    links = [];
    console.log('getPatientLinks');
    console.log(jQuery('#forum_40 a'));
    jQuery('#forum_40 .folder a').each(function(i, el) {
      links.push(jQuery(el).attr('href'));
      return jQuery(el).removeAttr('target');
    });
    console.log(JSON.stringify(links));
    return links;
  };

  opts.getPaginationNextLink = function() {
    var nextPage;
    nextPage = jQuery('.pages .next');
    $(nextPage[0]).attr('id', 'nextPage');
    if (nextPage.length === 0) return false;
    return '#nextPage';
  };

  opts.getPatientInfo = function() {
    var date, patientInfo;
    patientInfo = {};
    patientInfo.source = '摇篮网';
    date = jQuery('.con_info').text().match('\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{2}')[0];
    patientInfo.releaseDate = date.substring(0, 10);
    patientInfo.name = '无';
    patientInfo.securityId = '';
    patientInfo.gender = '';
    patientInfo.age = '';
    patientInfo.cellphone = jQuery('#infosidemain').text().match(/1[3|4|5|8][0-9]\d{4,8}/)[0];
    if (patientInfo.cellphone.length !== 11) patientInfo.cellphone = '';
    patientInfo.addr = '';
    patientInfo.qq = '';
    patientInfo.desc = jQuery('.thread_title h1').text();
    patientInfo.url = document.location.href;
    console.log(JSON.stringify(patientInfo));
    return patientInfo;
  };

  console.log('yaolan config is loaded');

}).call(this);

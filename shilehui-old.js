(function() {

  ﻿window.opts = {
    baseUrl: 'http://www.shilehui.com/',
    indexPath: '/AssistUI/Index.aspx?PassType=4',
    pageLimit: 1
  };

  opts.indexPage = opts.baseUrl + opts.indexPath;

  opts.getPatientLinks = function() {
    var links;
    links = [];
    $('.donate_info_content_title a').each(function(i, el) {
      links.push($(el).attr('href'));
      return $(el).removeAttr('target');
    });
    return links;
  };

  opts.getPaginationNextLink = function() {
    var nextPageImg, strNextPage;
    strNextPage = '';
    nextPageImg = $('#donate_main img[src*="Page_next.gif"]');
    if (nextPageImg.parent('a')) {
      strNextPage = nextPageImg.parent('a').attr('href');
    } else {
      strNextPage = '';
    }
    if (!strNextPage) return '';
    return strNextPage;
  };

  opts.getPatientInfo = function() {
    var date, dateTitle, patientInfo, proverEls, proverInfo, starterEls, starterInfo, userInfoList;
    window.isDoctor = function(pro) {
      var key, result, _i, _len, _ref;
      result = false;
      _ref = ['医', '放射', '血', '疗', '大夫', '儿科', '内科'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        key = _ref[_i];
        if (pro.indexOf(key) !== -1) {
          result = true;
          break;
        }
      }
      return result;
    };
    patientInfo = {};
    patientInfo.source = '施乐会';
    dateTitle = $('.topicMeta').find('span:contains("发布时间")');
    date = '';
    if (dateTitle.length > 0) {
      date = dateTitle.next('em').text();
      if (date) date = date.substring(0, 9);
      patientInfo.releaseDate = date;
    }
    userInfoList = $('#Left_Assist1_spanReceipt tr');
    userInfoList.each(function(i, el) {
      var addrTitle, ageTitle, cellphoneTitle, genderTitle, nameTitle, qqTitle, securityIdTitle;
      nameTitle = $(el).find('td:contains("名")');
      if (nameTitle.length > 0) patientInfo.name = nameTitle.next('td').text();
      securityIdTitle = $(el).find('td:contains("证")');
      if (securityIdTitle.length > 0) {
        patientInfo.securityId = securityIdTitle.next('td').text();
      }
      genderTitle = $(el).find('td:contains("性")');
      if (genderTitle.length > 0) {
        patientInfo.gender = genderTitle.next('td').text();
      }
      ageTitle = $(el).find('td:contains("龄")');
      if (ageTitle.length > 0) patientInfo.age = ageTitle.next('td').text();
      cellphoneTitle = $(el).find('td:contains("手")');
      if (cellphoneTitle.length > 0) {
        patientInfo.cellphone = cellphoneTitle.next('td').text();
      }
      cellphoneTitle = $(el).find('td:contains("话")');
      if (cellphoneTitle.length > 0) {
        patientInfo.cellphone = cellphoneTitle.next('td').text();
      }
      addrTitle = $(el).find('td:contains("址")');
      if (addrTitle.length > 0) patientInfo.addr = addrTitle.next('td').text();
      qqTitle = $(el).find('td:contains("Q")');
      if (qqTitle.length > 0) patientInfo.qq = qqTitle.next('td').text();
      patientInfo.desc = document.title;
      return patientInfo.url = document.location.href;
    });
    console.log(JSON.stringify(patientInfo));
    proverInfo = {};
    proverEls = $('#Left_Assist1_spanshow_wjy').parents('tr').find('tr');
    proverEls.each(function(i, el) {
      var cell, cellTitle, phoneTitle, pro, proTitle;
      proTitle = $(el).find('td:contains("职")');
      pro = proTitle.next('td').text();
      cellTitle = $(el).find('td:contains("机")');
      if (cellTitle) {
        cell = cellTitle.next('td').text();
        if (cell) proverInfo.cellphone = cell;
      }
      phoneTitle = $(el).find('td:contains("话")');
      if (!proverInfo.cellphone && phoneTitle) {
        cell = phoneTitle.next('td').text();
        if (cell) proverInfo.cellphone = cell;
      }
      if (window.isDoctor(pro)) {
        console.log('医生 exit!!!');
        proverInfo = {};
        return;
      }
      proverInfo.desc = '证明人 ' + pro;
      proverInfo.name = '无';
      proverInfo.source = '施乐会';
      proverInfo.url = document.location.href;
      return proverInfo.releaseDate = date;
    });
    starterInfo = {};
    starterEls = $('#Left_Assist1_spanshow').parents('tr').find('tr');
    starterEls.each(function(i, el) {
      var cell, cellTitle, pro, proTitle, telTitle;
      proTitle = $(el).find('td:contains("职")');
      pro = proTitle.next('td').text();
      cellTitle = $(el).find('td:contains("机")');
      telTitle = $(el).find('td:contains("话")');
      if (cellTitle) {
        cell = cellTitle.next('td').text();
        if (cell && cell !== '无') starterInfo.cellphone = cell;
      }
      if (!starterInfo.cellphone) {
        cell = telTitle.next('td').text();
        if (cell && cell !== '无') starterInfo.cellphone = cell;
      }
      if (isDoctor(pro)) {
        console.log('医生 exit!!!');
        starterInfo = {};
        return;
      }
      starterInfo.desc = '发起人 ' + pro;
      starterInfo.name = '无';
      starterInfo.source = '施乐会';
      starterInfo.url = document.location.href;
      return starterInfo.releaseDate = date;
    });
    return [patientInfo, proverInfo, starterInfo];
  };

}).call(this);

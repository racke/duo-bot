(function() {
  var baseUrl, beforeStart, casper, configFile, donateMain, fs, getPaginationNextLink, getPatientInfo, getPatientInfosInList, getPatientLinks, indexPage, indexPath, needExit, nextPageImg, nextPageLink, pageIdx, pageLimit, patientInfos, patientLinks, processedLinks, server_base, util;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  util = require("utils");

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js', 'common.js'],
    logLevel: 'debug',
    verbose: true,
    onResourceReceived: function(req) {},
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false,
      javascriptEnabled: false
    }
  });

  console.log('casper created');

  console.log('passed config file ');

  configFile = casper.cli.options.config;

  console.log('get configs from ' + configFile);

  phantom.injectJs(configFile);

  baseUrl = opts.baseUrl;

  indexPath = opts.indexPath;

  indexPage = baseUrl + indexPath;

  console.log('index page: ' + indexPage);

  pageLimit = opts.pageLimit;

  beforeStart = opts.beforeStart;

  getPaginationNextLink = opts.getPaginationNextLink;

  getPatientInfo = opts.getPatientInfo;

  getPatientLinks = opts.getPatientLinks;

  patientInfos = [];

  processedLinks = [];

  nextPageLink = '';

  nextPageImg = '';

  donateMain = '';

  needExit = false;

  pageIdx = 0;

  casper.start(indexPage, function() {
    return console.log('started');
  });

  if (beforeStart) {
    casper.wait(1000, function() {
      var link;
      link = beforeStart(casper);
      if (link) return casper.click(link);
    });
  }

  casper.then(function() {
    return casper.wait(1000, getPatientInfosInList);
  });

  casper.then(function() {
    console.log('patient links');
    console.log(patientLinks);
    return patientLinks.forEach(function(link, i) {
      return casper.then(function() {
        var url;
        url = link;
        if (link.indexOf('http') === -1) url = baseUrl + link;
        if (baseUrl === 'http://127.0.0.1:8080') {
          url = 'http://bbs.yaolan.com/' + link;
        }
        console.log('opening patient link ' + url);
        return casper.thenOpen(url, function() {
          console.log('now in page' + this.getTitle());
          return patientInfos.push(casper.evaluate(getPatientInfo));
        });
      });
    });
  });

  /*    
  getNextPageLink= ->
      $('a:contains(下一页)').attr('id', 'nextPage')
      isDisabled = $('a:contains(下一页)').attr('disabled')
      console.log 'isDisabled: '+isDisabled
      if isDisabled then return false
      return '#nextPage'
  */

  patientLinks = [];

  getPatientInfosInList = function(isStart) {
    if (isStart == null) isStart = false;
    pageIdx++;
    console.log('page index is ' + pageIdx);
    patientLinks = patientLinks.concat(casper.evaluate(getPatientLinks));
    nextPageLink = casper.evaluate(getPaginationNextLink);
    console.log('nextPageLink is ' + nextPageLink);
    if (nextPageLink === false || pageIdx > pageLimit) {
      console.log('no next page or exceed page limit, end paging');
      return;
    }
    console.log('click ' + nextPageLink);
    casper.click(nextPageLink);
    return casper.wait(1000, function() {
      return getPatientInfosInList();
    });
  };

  server_base = 'http://w74164.s74.chinaccnet.cn';

  casper.then(function() {
    var strPatientInfos;
    strPatientInfos = JSON.stringify(patientInfos);
    console.log(strPatientInfos);
    return casper.open(server_base + '/duo-bot/index.php?r=patient/createPatients', {
      method: 'post',
      data: {
        'data': strPatientInfos
      }
    }).then(function() {
      return this.debugHTML();
    });
  });

  fs = require('fs');

  casper.run(function() {
    var resultFile, strPatientInfos;
    strPatientInfos = JSON.stringify(patientInfos);
    fs.touch('./output');
    resultFile = fs.open('./output', 'w');
    resultFile.write(strPatientInfos);
    resultFile.close();
    return this.exit();
  });

}).call(this);

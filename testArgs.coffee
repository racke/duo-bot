phantom.casperPath = 'casperjs'
phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js')
#parentInfo {name:'', desc:'', sid:'', cellphone: '', url: ''}
patientInfos = []
processedLinks = []
nextPageLink = ''
casper = require('casper').create(
    clientScripts: [
        'libs/jquery-1.7.1.min.js'
    ]
    logLevel: 'info'
    verbose: true
    onError: (self, m)->
        console.log 'FATAL:' + m
        self.exit()
    pageSetting:
        loadImages: false
        loadPlugins: false

)
console.log 'casper created'
console.log 'passed options '
require("utils").dump casper.cli.options
opts = casper.cli.options
console.log opts.config
phantom.injectJs(opts.config)
console.log baseUrl

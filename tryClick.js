(function() {
  var casper, link, titleEls, titles, url;

  phantom.casperPath = '../casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  titles = [];

  titleEls = [];

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    verbose: 'true',
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  url = 'http://127.0.0.1/mobilePoc/index.html';

  casper.start(url, function() {});

  link = '';

  casper.then(function() {
    return link = casper.evaluate(function() {
      titles = [];
      link = $('#test').attr('href');
      return link;
    });
  });

  casper.thenOpen('http://127.0.0.1/mobilePoc/' + link, function() {
    return console.log(this.getTitle());
  });

  /*
  casper.then ->
      link = link
      @.click "a[href='#{link}']"
      @.wait 100, ->
          @.evaluate ->
              console.log document.title
  */

  casper.run(function() {
    console.log(link);
    return this.exit();
  });

}).call(this);

(function() {
  var casper, nextPageLink, opts, patientInfos, processedLinks;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  patientInfos = [];

  processedLinks = [];

  nextPageLink = '';

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'info',
    verbose: true,
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('casper created');

  console.log('passed options ');

  require("utils").dump(casper.cli.options);

  opts = casper.cli.options;

  console.log(opts.config);

  phantom.injectJs(opts.config);

  console.log(baseUrl);

}).call(this);

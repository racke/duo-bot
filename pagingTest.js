(function() {
  var casper;

  phantom.casperPath = 'casperjs';

  phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js');

  casper = require('casper').create({
    clientScripts: ['libs/jquery-1.7.1.min.js'],
    logLevel: 'debug',
    verbose: true,
    onError: function(self, m) {
      console.log('FATAL:' + m);
      return self.exit();
    },
    pageSetting: {
      loadImages: false,
      loadPlugins: false
    }
  });

  console.log('before start');

  casper.start('http://www.ikcw.com/jiuzhu/', function() {
    return console.log('started');
  });

  casper.thenEvaluate(function() {
    return $('a:contains(下一页)').attr('id', 'nextPage');
  });

  casper.thenClick('#nextPage').thenEvaluate(function() {
    var pageNum;
    pageNum = $('#csjz_index_left_wyqiuzhu_AspNetPager1 span').text();
    console.log('in page ' + pageNum);
    return $('a:contains(下一页)').attr('id', 'nextPage');
  });

  casper.thenClick('#nextPage').thenEvaluate(function() {
    var pageNum;
    pageNum = $('#csjz_index_left_wyqiuzhu_AspNetPager1 span').text();
    console.log('in page ' + pageNum);
    return $('a:contains(下一页)').attr('id', 'nextPage');
  });

  casper.back();

  casper.thenEvaluate(function() {
    var pageNum;
    pageNum = $('#csjz_index_left_wyqiuzhu_AspNetPager1 span').text();
    return console.log('in page ' + pageNum);
  });

  casper.run(function() {
    console.log('run');
    return casper.exit();
  });

}).call(this);

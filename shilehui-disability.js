(function() {

  window.opts = {
    baseUrl: 'http://www.shilehui.com/',
    pageLimit: 1,
    indexPath: 'AssistUI/Index.aspx?PassType=8&Sid=-2&AgeS=-1&Province='
  };

  opts.getPatientLinks = function() {
    var links;
    links = [];
    $('.donate_info_content_title a').each(function(i, el) {
      links.push($(el).attr('href'));
      return $(el).removeAttr('target');
    });
    return links;
  };

  opts.getPaginationNextLink = function() {
    var nextPageImg, strNextPage;
    strNextPage = '';
    nextPageImg = $('#donate_main img[src*="Page_next.gif"]');
    if (nextPageImg.parent('a')) {
      strNextPage = nextPageImg.parent('a').attr('href');
    } else {
      strNextPage = '';
    }
    if (!strNextPage) return '';
    return strNextPage;
  };

  opts.getPatientInfo = function() {
    var date, dateTitle, patientInfo, userInfoList;
    patientInfo = {};
    patientInfo.source = '施乐会';
    dateTitle = $('.topicMeta').find('span:contains("发布时间")');
    if (dateTitle.length > 0) {
      date = dateTitle.next('em').text();
      if (date) date = date.substring(0, 9);
      patientInfo.releaseDate = date;
    }
    userInfoList = $('#Left_Assist1_spanReceipt tr');
    userInfoList.each(function(i, el) {
      var addrTitle, ageTitle, cellphoneTitle, genderTitle, nameTitle, qqTitle, securityIdTitle;
      nameTitle = $(el).find('td:contains("名")');
      if (nameTitle.length > 0) patientInfo.name = nameTitle.next('td').text();
      securityIdTitle = $(el).find('td:contains("证")');
      if (securityIdTitle.length > 0) {
        patientInfo.securityId = securityIdTitle.next('td').text();
      }
      genderTitle = $(el).find('td:contains("性")');
      if (genderTitle.length > 0) {
        patientInfo.gender = genderTitle.next('td').text();
      }
      ageTitle = $(el).find('td:contains("龄")');
      if (ageTitle.length > 0) patientInfo.age = ageTitle.next('td').text();
      cellphoneTitle = $(el).find('td:contains("手")');
      if (cellphoneTitle.length > 0) {
        patientInfo.cellphone = cellphoneTitle.next('td').text();
      }
      cellphoneTitle = $(el).find('td:contains("话")');
      if (cellphoneTitle.length > 0) {
        patientInfo.cellphone = cellphoneTitle.next('td').text();
      }
      addrTitle = $(el).find('td:contains("址")');
      if (addrTitle.length > 0) patientInfo.addr = addrTitle.next('td').text();
      qqTitle = $(el).find('td:contains("Q")');
      if (qqTitle.length > 0) patientInfo.qq = qqTitle.next('td').text();
      patientInfo.desc = document.title;
      return patientInfo.url = document.location.href;
    });
    console.log(JSON.stringify(patientInfo));
    return patientInfo;
  };

}).call(this);

#path to your casper js https://github.com/n1k0/casperjs 
phantom.casperPath = 'casperjs'
phantom.injectJs(phantom.casperPath + '/bin/bootstrap.js')
titles = []
titleEls = []
casper = require('casper').create(
    clientScripts: [
        'libs/jquery-1.7.1.min.js'
    ]
    logLevel: 'info'
    onError: (self, m)->
        console.log 'FATAL:' + m
        self.exit()
    pageSetting:
        loadImages: false
        loadPlugins: false

)
console.log 'casper created'
#url = 'http://google.com/'
url = 'http://127.0.0.1/mobilePoc/index.html'
casper.start(url, ()->
    #@.fill('form[name=f]', {q: 'casperjs'}, true)
)
console.log 'titles '+titles
casper.then(->
    titles = casper.evaluate(()->
        titles = []
        titleEls = $('h3.r a')
        titleEls.each((i, el)->
            titles.push $(el).text()
        )
        return titles
        #return titleEls
    )
)
casper.run(->
    console.log titles
)
